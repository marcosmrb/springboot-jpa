package br.com.jpa.poc.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.jpa.poc.domain.ContaCorrenteDTO;
import br.com.jpa.poc.entity.ContaCorrente;

@Repository
public interface ContaCorrenteRepository extends JpaRepository<ContaCorrente, Integer> {

    @Query(nativeQuery = true, 
        value = "SELECT p.cpf, p.nome, c.tipo_cc, c.conta FROM cc_cc c INNER JOIN bc_pessoa p on c.primeiro_titular = p.id_pessoa WHERE c.id_cc = :id_cc")
    List<ContaCorrenteDTO> buscaConta(@Param("id_cc") Integer id);
}