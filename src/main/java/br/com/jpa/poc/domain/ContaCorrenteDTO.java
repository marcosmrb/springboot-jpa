package br.com.jpa.poc.domain;

public interface ContaCorrenteDTO {
    String getCpf();
    String getNome();
    Integer getTipoConta();
    Integer getConta();
}