package br.com.jpa.poc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jpa.poc.domain.ContaCorrenteDTO;
import br.com.jpa.poc.repository.ContaCorrenteRepository;

@Service
public class ContaCorrenteService{

    @Autowired
    private ContaCorrenteRepository contaCorrenteRepository;

    public ContaCorrenteDTO find(Integer id){
        List<ContaCorrenteDTO> contaCorrenteList = contaCorrenteRepository.buscaConta(id);
        ContaCorrenteDTO contaCorrente = contaCorrenteList.stream().findFirst().get();
        return contaCorrente;
    }


}