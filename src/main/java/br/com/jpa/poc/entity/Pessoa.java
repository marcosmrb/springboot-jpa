package br.com.jpa.poc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity @Getter @Setter
@Table(name="bc_pessoa")
public class Pessoa {

    @JsonIgnore
    @Id
    @Column(name = "id_pessoa")
    private Integer id;
    @Column(name = "cpf")
    private String cpf;
    @Column(name = "nome")
    private String nome;

    @JsonIgnore
    @OneToOne(mappedBy = "pessoa")
    //@JoinColumn(name = "id_pessoa")
    // @JoinColumns(foreignKey = @ForeignKey(name = "primeiro_titular"), 
    //     value = { 
    //             @JoinColumn(name = "id_pessoa", referencedColumnName = "primeiro_titular")
    //             })
    private ContaCorrente contaCorrente;

    public Pessoa(){

    }

}

