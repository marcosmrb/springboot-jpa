package br.com.jpa.poc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity @Getter @Setter
@Table(name="cc_cc")
public class ContaCorrente {

    @Id
    @Column(name = "id_cc")
    private Integer id;
    @Column(name = "tipo_cc")
    private String tipoConta;
    @Column(name = "conta")
    private Integer conta;

    // @Column(name = "primeiro_titular")
    // private Integer titular;

    @OneToOne
    //@OneToOne(optional = false)
    @JoinColumn(name="primeiro_titular")
    private Pessoa pessoa;

    public ContaCorrente(){

    }


}

