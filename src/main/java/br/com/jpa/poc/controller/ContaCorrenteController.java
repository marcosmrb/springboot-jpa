package br.com.jpa.poc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.jpa.poc.domain.ContaCorrenteDTO;
import br.com.jpa.poc.entity.ContaCorrente;
import br.com.jpa.poc.service.ContaCorrenteService;

@RestController
@RequestMapping(value = "/contacorrente")
public class ContaCorrenteController{

    @Autowired
    private ContaCorrenteService contaCorrenteService;

    @GetMapping(value="/{id}")
    public ResponseEntity<ContaCorrenteDTO> find(@PathVariable Integer id) {
        ContaCorrenteDTO contaCorrente = contaCorrenteService.find(id);

        return ResponseEntity.ok().body(contaCorrente);
    }

}